class LikePost < ApplicationRecord
  #relações
  belongs_to :user
  belongs_to :post

  #validações
  validate :already_liked?

  #métodos/funções
  def already_liked?
    if post.users.include?(user) #verifica se tem usuario nessa lista ou pode tb fazer (LikePost.find_by(user_id: user.id, post_id: post.id)).present?
      errors.add(:already_liked, "Voce ja deu like nesse post")
    end
  end

end
