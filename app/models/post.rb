class Post < ApplicationRecord
  #relações
  belongs_to :user                        #relação: tem um usuário
  has_many :like_posts                    #relação: tem diversas curtidas
  has_many :users, through: :like_posts   #relação: diversos usuários dão diversos likes
  #validações
  validates :content_text, presence: true, length: {maximum: 256}
  
  #métodos/funções
end
