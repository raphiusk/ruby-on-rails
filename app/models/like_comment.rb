class LikeComment < ApplicationRecord
  belongs_to :comment
  belongs_to :user

    #validações
    validate :already_liked?

    #métodos/funções
    def already_liked?
      if comment.users.include?(user) #verifica se tem usuario nessa lista ou pode tb fazer (LikePost.find_by(user_id: user.id, post_id: post.id)).present?
        errors.add(:already_liked, "Voce ja deu like nesse comentario")
      end
    end
end
