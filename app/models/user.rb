class User < ApplicationRecord
    #relações
        has_many :posts
        has_many :like_posts
        has_many :comments
        #has_many :liked_posts, class_name:"Post", through: :like_posts  #qtde de likes setando pra classe posts e linkando com like_posts lá
        
        #tem e pertence a muitos: ... , através da tabela, com a chave-estrangeira..., a classe ...., 
        has_and_belongs_to_many :followers, join_table: "follows", foreign_key: "followed_id", class_name: "User", association_foreign_key: "follower_id"
        has_and_belongs_to_many :followeds, join_table: "follows", foreign_key: "follower_id", class_name: "User", association_foreign_key: "followed_id"
        
    #validações
        validates :name, :email, :birthdate, :gender, :contact_phone, :nickname, presence: true
        validates :email, uniqueness: true      #email único UNIQUE buscar por verificar se tem arroba ou não
        validates :contact_phone, length: {is: 11}, numericality: {only_integer: true}  #contact_phone contém 11 caracteres e apenas inteiro
        validate :under_age?
        
        #enum é uma hash
        enum gender:{
            "F":0,
            "M":1
        }

    #Métodos/funções
        #Verificar se é maior de 18
        def age
            ((Date.today-birthdate).to_i/365.25).to_i
        end

        #Verifica se tem acesso ou não
        def under_age?
            if age < 18
                errors.add(:under_age, "Sai bebe")            
            end
        end
    end
