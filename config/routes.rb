Rails.application.routes.draw do
  post 'comments/add_like', to: "comments#create_like"
  post 'comments', to: "comments#create"
  get 'comments', to: "comments#index"
  get 'comments/:id', to: "comments#show"
  put 'comments/update', to: "comments#update"
  delete 'comments/:id', to: "comments#destroy"
  
  post 'posts', to: "posts#create"
  post 'posts/add_like', to: "posts#create_like"
  get 'posts', to: "posts#index"
  get 'posts/:id', to: "posts#show"
  put 'posts/update', to: "posts#update"
  delete 'posts/:id', to: "posts#destroy"
  
  post 'users', to: "user#create"
  get 'users', to: "user#index"
  get 'users/:id', to: "users#show" 
  put 'users/:id', to: "users#update"
  delete 'users/:id', to: "users#destroy"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
